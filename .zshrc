#Options
setopt AUTO_CD
setopt NO_CASE_GLOB
setopt EXTENDED_HISTORY
setopt SHARE_HISTORY
setopt HIST_VERIFY
setopt APPEND_HISTORY
setopt CORRECT
setopt CORRECT_ALL

#Variable
ZSH_AUTOSUGGEST_STRATEGY=( completion history )

#HISTORY
HISTSIZE=20000
SAVEHIST=50000
HISTFILE=~/.zsh_history

#style
autoload -U compinit && compinit
zstyle ':completion:*' menu select=1
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on

#Alias
alias history="history 1"
alias ls="ls -G"
alias ll="ls -als"
alias lll="ls -ales"
alias hi="history | tail -n $1"
alias hg="history | grep -i $1"
alias pg="ps aucx | head -n 1; ps aucx | grep -i $1"
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias autopkgr-bg='/Applications/AutoPkgr.app/Contents/MacOS/AutoPkgr -runInBackground YES'

#GIT
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
#RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats '%F{240}(%b)%r%f'
zstyle ':vcs_info:*' enable git

#Path
export PATH=~/bin:$PATH

#Prompt
PROMPT="%(?.%F{green}√.%F{red}X%?)%f %(!.%B%F{red}%n%f%F{075}@%m%f%b[%3~]%#.%B%F{075}%n@%m%f%b[%3~]%#)"
RPROMPT=\$vcs_info_msg_0_' %*'

#Submodules
if [[ -r ~/.dotfiles/submodule/zsh-autosuggestions/zsh-autosuggestions.zsh ]]; then
source ~/.dotfiles/submodule/zsh-autosuggestions/zsh-autosuggestions.zsh
fi 

#local override this must stay the last line in this file
[ -f ".zshrc.local" ] && source ".zshrc.local"

#muss als letztes stehen
if [[ -r ~/.dotfiles/submodule/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]]; then
source ~/.dotfiles/submodule/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi
