#Notes for dotfiles
##installation
```
ln -sv ~/.dotfiles/.vimrc ~
ln -sv ~/.dotfiles/.zshrc ~
```
##custom color propt. change the color by altering the number
```
echo 'PROMPT="%(?.%F{green}√.%F{red}X%?)%f %(!.%B%F{red}%n%f%F{075}@%m%f%b[%3~]%#.%B%F{075}%n@%m%f%b[%3~]%#) "' >> ~/.zshrc.local
```

##installation pubkeys
```
mkdir ~/.ssh; curl https://mon.itwrx.de/keys/pubkeys.txt >> ~/.ssh/authorized_keys
```

##submodules
fuer die submodules müssen folgende commands eingegeben werden
```
git submodule update --init --recursive
```
updaten mit
```
git submodule update --init --remote
```
neue submodule können mit 
```
git submodule add https://github.com/anishathalye/dotbot dotbot
```
hinzugefügt werden
